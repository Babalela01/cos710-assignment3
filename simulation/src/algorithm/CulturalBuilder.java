package algorithm;

import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.util.AlgorithmBuilder;

/**
 * @author renet
 *         on 2016/04/29.
 */
public class CulturalBuilder implements AlgorithmBuilder<CulturalAlgorithm2> {
    private DoubleProblem problem;
    private int maxEvaluations;
    private int maxPopulationSize;
    private int maxArhiveSize;
    private double deviation;
    private int bisections;

    public CulturalBuilder(DoubleProblem problem, int maxEvaluations, int maxPopulationSize, int maxArhiveSize, double deviation, int bisections) {
        this.problem = problem;
        this.maxEvaluations = maxEvaluations;
        this.maxPopulationSize = maxPopulationSize;
        this.maxArhiveSize = maxArhiveSize;
        this.deviation = deviation;
        this.bisections = bisections;
    }


    public CulturalAlgorithm2 build() {
        return new CulturalAlgorithm2(maxEvaluations, maxPopulationSize, problem, bisections, deviation, maxArhiveSize);
    }


}
