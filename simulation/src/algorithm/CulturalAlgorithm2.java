//  CulturalAlgoritm.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

//package jmetal.metaheuristics.CulturalAlgorithm;

package algorithm;


import org.uma.jmetal.algorithm.impl.AbstractEvolutionStrategy;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.util.RepairDoubleSolution;
import org.uma.jmetal.solution.util.RepairDoubleSolutionAtBounds;
import org.uma.jmetal.util.AdaptiveGrid;
import org.uma.jmetal.util.SolutionListUtils;
import org.uma.jmetal.util.SolutionUtils;
import org.uma.jmetal.util.archive.impl.AdaptiveGridArchive;
import org.uma.jmetal.util.archive.impl.NonDominatedSolutionListArchive;
import util.DominanceRandomOs;

import java.util.*;

/**
 * Implementation of the Cultural Algorithm.
 */

public class CulturalAlgorithm2 extends AbstractEvolutionStrategy<DoubleSolution, List<DoubleSolution>> {

    private  Comparator<DoubleSolution> comparator = new DominanceRandomOs(3);
    private int bisections; //si
    private int maxEvaluations;  //Gmax
    private int evaluations = 0;
    private CABelief belief;
    private Random random = new Random();

    private double mutationRate; //1/n
    private double deviation; //1
    private int maxArchiveSize; //q
    private int tournamnetSize; //c

    private RepairDoubleSolution repair = new RepairDoubleSolutionAtBounds();

    public CulturalAlgorithm2(int maxEvaluations, int maxPopulationSize, DoubleProblem problem, int bisections, double deviation, int maxArchiveSize) {
        super(problem);
        this.maxEvaluations = maxEvaluations;
        this.bisections = bisections;
        this.mutationRate = 1.0/problem.getNumberOfVariables();
        this.deviation = deviation;
        this.maxArchiveSize = maxArchiveSize;
        this.tournamnetSize = maxPopulationSize/2;

        setMaxPopulationSize(maxPopulationSize);
    }

    @Override
    protected void initProgress() {
        evaluations = 0;
        belief = intiBelief();
    }

    private CABelief intiBelief() {
        return new CABelief(getPopulation());
    }


    @Override
    protected void updateProgress() {
        evaluations++;
    }

    @Override
    protected boolean isStoppingConditionReached() {
        return evaluations >= maxEvaluations;
    }

    protected List<DoubleSolution> createInitialPopulation() {
        Problem<DoubleSolution> problem = getProblem();
        int maxPopulationSize = getMaxPopulationSize();
        List<DoubleSolution> solutionList = new ArrayList<>(maxPopulationSize);
        for (int i = 0; i < maxPopulationSize; ++i) {
            solutionList.add(problem.createSolution());
        }
        return solutionList;
    }

    protected List<DoubleSolution> evaluatePopulation(List<DoubleSolution> population) {
        Problem<DoubleSolution> p = getProblem();
        population.forEach(p::evaluate);
        return population;
    }

    @Override
    protected List<DoubleSolution> selection(List<DoubleSolution> solutionList) {
        return solutionList;
    }

    private DoubleSolution tournament(AdaptiveGrid<DoubleSolution> grid, DoubleSolution candidate, DoubleSolution competitor) {
        int flag = comparator.compare(candidate, competitor);
        if (flag < 0) {
            return candidate;
        } else if (flag == 0) {
           int iCandidate = grid.location(candidate);
           int iCompetitor = grid.location(competitor);
            if (iCandidate == -1 || iCompetitor == -1) {
                if (iCandidate == -1 && iCompetitor == -1) {
                    return SolutionUtils.getBestSolution(candidate, competitor, comparator); //Random
                } else if (iCandidate == -1) {
                    return candidate;
                } else {
                    return competitor;
                }
            } else {
                int popCand = grid.getLocationDensity(iCandidate);
                int popComp = grid.getLocationDensity(iCompetitor);

                if (popComp == popCand) {
                    return SolutionUtils.getBestSolution(candidate, competitor, comparator); //Random
                } else if (popCand < popComp) {
                    return candidate;
                } else {
                    return competitor;
                }
            }
        } else {
            return competitor;
        }
    }

    private void wins(HashMap<DoubleSolution, Integer> countermap, DoubleSolution candidate) {
        countermap.put(candidate, countermap.getOrDefault(candidate, 0) + 1);
    }

    @Override
    protected List<DoubleSolution> reproduction(List<DoubleSolution> population) {
        adjustBelief(population);
        return variate(population);
    }

    private void adjustBelief(List<DoubleSolution> accepted) {
        if (evaluations > 0) {
            belief.update(accepted);
        }
    }

    private List<DoubleSolution> variate(List<DoubleSolution> population) {
        List<DoubleSolution> newpop = new LinkedList<>();
        for (DoubleSolution s : population) {
            DoubleSolution newS = (DoubleSolution) s.copy();
            for (int i=0; i<s.getNumberOfVariables(); ++i) {
                if (random.nextDouble() < mutationRate) {
                    double yl = newS.getLowerBound(i) ;
                    double yu = newS.getUpperBound(i) ;
                    double y = newS.getVariableValue(i) + random.nextGaussian() * deviation;
                    newS.setVariableValue(i, repair.repairSolutionVariableValue(y,yl,yu));
                }
            }
            newpop.add(newS);
        }
        return newpop;
    }

    protected List<DoubleSolution> replacement(List<DoubleSolution> population, List<DoubleSolution> offspringPopulation) {
        offspringPopulation.addAll(population);

        int maxPopulationSize = getMaxPopulationSize();
        HashMap<DoubleSolution, Integer> countermap = new HashMap<>(maxPopulationSize *2);
        AdaptiveGrid<DoubleSolution> grid = belief.archive.getGrid();
        for (DoubleSolution candidate : offspringPopulation) {
            List<DoubleSolution> competitors = SolutionListUtils.selectNRandomDifferentSolutions(tournamnetSize, offspringPopulation);
            for (DoubleSolution competitor: competitors) {
                wins(countermap,tournament(grid, candidate, competitor));
            }
        }
        offspringPopulation.sort((a, b) -> Integer.compare(countermap.getOrDefault(a,0), countermap.getOrDefault(b,0)));
        return offspringPopulation.subList(0, maxPopulationSize);
    }


    public List<DoubleSolution> getResult() {

        return SolutionListUtils.getNondominatedSolutions(belief.archive.getSolutionList());

    }

    @Override
    public void run() {
        super.run();

        System.gc();
    }

    @Override
    public String getName() {
        return "CA";
    }

    @Override
    public String getDescription() {
        return "Cultural Algorithm";
    }

    public class CABelief {

        private AdaptiveGridArchive<DoubleSolution> archive;

        public CABelief(List<DoubleSolution> solutions) {
            archive = new AdaptiveGridArchive<>(maxArchiveSize, bisections, getProblem().getNumberOfObjectives());
            archive.setDominanceComparator(comparator);
            for (DoubleSolution solution : solutions) {
                archive.add(solution);
            }
        }

        public boolean add(DoubleSolution solution) {
            return archive.add(solution);
        }

        public void update(List<DoubleSolution> solutions) {
            solutions.forEach(this::add);
        }
    }
}
