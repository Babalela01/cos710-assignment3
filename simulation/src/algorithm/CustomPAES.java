package algorithm;

import org.uma.jmetal.util.SolutionListUtils;
import org.uma.jmetal.util.archive.impl.NonDominatedSolutionListArchive;
import util.DominanceRandomOs;
import org.uma.jmetal.algorithm.multiobjective.paes.PAES;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.List;

/**
 * @author renet
 *         on 2016/04/29.
 */
public class CustomPAES extends PAES<DoubleSolution>{


    /**
     * Constructor
     */
    public CustomPAES(Problem<DoubleSolution> problem, int archiveSize, int maxEvaluations, int biSections, MutationOperator<DoubleSolution> mutationOperator) {
        super(problem, archiveSize, maxEvaluations, biSections, mutationOperator);
        this.comparator =new DominanceRandomOs(3);
        //this.archive.setDominanceComparator(comparator);
    }

    @Override
    public List<DoubleSolution> getResult() {
        return SolutionListUtils.getNondominatedSolutions(super.getResult());
    }

}
