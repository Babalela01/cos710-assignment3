package algorithm;

import org.uma.jmetal.algorithm.multiobjective.moead.AbstractMOEAD;
import org.uma.jmetal.algorithm.multiobjective.moead.MOEADBuilder;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;

/**
 * @author renet
 *         on 2016/04/29.
 */
public class CustomMOEABuilder extends MOEADBuilder {
    /**
     * Constructor
     *
     * @param problem
     * @param variant
     */
    public CustomMOEABuilder(Problem<DoubleSolution> problem, Variant variant) {
        super(problem, variant);
    }

    @Override
    public AbstractMOEAD<DoubleSolution> build() {
        return new CustomMOEAD(problem, populationSize,
                resultPopulationSize, maxEvaluations, mutation, crossover,
                functionType, dataDirectory, neighborhoodSelectionProbability,
                maximumNumberOfReplacedSolutions, neighborSize);
    }
}
