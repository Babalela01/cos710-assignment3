package algorithm;

import org.uma.jmetal.util.SolutionListUtils;
import org.uma.jmetal.util.archive.impl.NonDominatedSolutionListArchive;
import util.DominanceRandomOs;
import org.uma.jmetal.algorithm.multiobjective.moead.MOEAD;
import org.uma.jmetal.algorithm.multiobjective.moead.util.MOEADUtils;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;
import util.UniformDesignGenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author renet
 *         on 2016/04/29.
 */
public class CustomMOEAD extends MOEAD {

    DominanceRandomOs comparator = new DominanceRandomOs(3);
    static volatile Map<Integer, List<double[]>> lamdamap = new HashMap<>(4);
    static final Lock l = new ReentrantLock();

    UniformDesignGenerator ud;

    public CustomMOEAD(Problem<DoubleSolution> problem, int populationSize, int resultPopulationSize, int maxEvaluations, MutationOperator<DoubleSolution> mutation, CrossoverOperator<DoubleSolution> crossover, FunctionType functionType, String dataDirectory, double neighborhoodSelectionProbability, int maximumNumberOfReplacedSolutions, int neighborSize) {
        super(problem, populationSize, resultPopulationSize, maxEvaluations, mutation, crossover, functionType, dataDirectory, neighborhoodSelectionProbability, maximumNumberOfReplacedSolutions, neighborSize);
        ud = new UniformDesignGenerator(problem.getNumberOfObjectives(), populationSize);
    }



    @Override
    protected void initializeUniformWeight() {
        List<double[]> var;
        int objectives = problem.getNumberOfObjectives();
        if (!lamdamap.containsKey(objectives)) {
            l.lock();
                if (!lamdamap.containsKey(objectives)) {
                    List<double[]> var1 =ud.generate();
                    lamdamap.put(objectives, var1);
                }
            l.unlock();
        }
        var = lamdamap.get(objectives);
        if (var.size() != lambda.length)
            throw new JMetalException("Wrong Size");
        for (int i=0; i<lambda.length; ++i) {
            lambda[i] = var.get(i).clone();
        }
    }

    /**
     * Overridend update neighbour hood function because fitnessFunction cannot be overridden.
     * @param individual
     * @param subProblemId
     * @param neighborType
     * @throws JMetalException
     */
    @Override
    protected  void updateNeighborhood(DoubleSolution individual, int subProblemId, NeighborType neighborType) throws JMetalException {
        int size;
        int time;

        time = 0;

        if (neighborType == NeighborType.NEIGHBOR) {
            size = neighborhood[subProblemId].length;
        } else {
            size = population.size();
        }
        int[] perm = new int[size];

        MOEADUtils.randomPermutation(perm, size);

        for (int i = 0; i < size; i++) {
            int k;
            if (neighborType == NeighborType.NEIGHBOR) {
                k = neighborhood[subProblemId][perm[i]];
            } else {
                k = perm[i];
            }
            double f1, f2;

            f1 = fitnessFunction(population.get(k), lambda[k]);
            f2 = fitnessFunction(individual, lambda[k]);

            if (f2 < f1) {
                population.set(k, (DoubleSolution) individual.copy());
                time++;
            }

            if (time >= maximumNumberOfReplacedSolutions) {
                return;
            }
        }
    }

    //@Override
    double fitnessFunction(DoubleSolution individual, double[] lambda) throws JMetalException {
        double fitness;

        double d1, d2, nl;
        double theta = 5.0;

        d1 = d2 = nl = 0.0;

        Set<Integer> chosenObjectives = comparator.getRandomIntegers(problem.getNumberOfObjectives());

        for (int i : chosenObjectives) {
            d1 += (individual.getObjective(i) - idealPoint[i]) * lambda[i];
            nl += Math.pow(lambda[i], 2.0);
        }
        nl = Math.sqrt(nl);
        d1 = Math.abs(d1) / nl;

        for (int i : chosenObjectives) {
            d2 += Math.pow((individual.getObjective(i) - idealPoint[i]) - d1 * (lambda[i] / nl), 2.0);
        }
        d2 = Math.sqrt(d2);

        fitness = (d1 + theta * d2);

        return fitness;
    }

    @Override
    public List<DoubleSolution> getResult() {
        return SolutionListUtils.getNondominatedSolutions(super.getResult());
    }
}
