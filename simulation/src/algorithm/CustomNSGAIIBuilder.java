package algorithm;

import util.DominanceRandomOs;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;

/**
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class CustomNSGAIIBuilder extends NSGAIIBuilder<DoubleSolution> {

  /**
   * NSGAIIBuilder constructor
   *
   * @param problem
   * @param crossoverOperator
   * @param mutationOperator
   */
  public CustomNSGAIIBuilder(Problem<DoubleSolution> problem, CrossoverOperator<DoubleSolution> crossoverOperator, MutationOperator<DoubleSolution> mutationOperator) {
    super(problem, crossoverOperator, mutationOperator);
    setSelectionOperator(new BinaryTournamentSelection<>(new DominanceRandomOs(3)));
  }
}
