//  DTLZ1.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package problems;

import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.problem.multiobjective.dtlz.DTLZ2;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of DTLZ2 that scales objective function values with 10^(i-1)
 */
public class DTLZ2B extends DTLZ2 {

  public DTLZ2B() throws ClassNotFoundException, JMetalException {
      super();
      setName("DTLZ2B");
  }

  public DTLZ2B(Integer numberOfVariables, Integer numberOfObjectives) throws JMetalException {
    super(numberOfVariables, numberOfObjectives);
      setName("DTLZ2B");
  }

  /** Evaluate() method */
  public void evaluate(DoubleSolution solution) {
    super.evaluate(solution);
    int numberOfObjectives = getNumberOfObjectives();

    for (int i = 0; i < numberOfObjectives; i++) {
      solution.setObjective(i, solution.getObjective(i) * Math.pow(10, i-1));
    }
  }
}

