package util;

import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.comparator.DominanceComparator;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author renet
 *         on 2016/04/29.
 */
public class DominanceRandomOs extends DominanceComparator<DoubleSolution> {
    private final int choices;

    public DominanceRandomOs(int choices) {
        this.choices = choices;
    }

    @Override
    public int compare(DoubleSolution solution1, DoubleSolution solution2) {
        if (solution1 == null) {
            throw new JMetalException("Solution1 is null") ;
        } else if (solution2 == null) {
            throw new JMetalException("Solution2 is null") ;
        } else if (solution1.getNumberOfObjectives() != solution2.getNumberOfObjectives()) {
            throw new JMetalException("Cannot compare because solution1 has " +
                    solution1.getNumberOfObjectives()+ " objectives and solution2 has " +
                    solution2.getNumberOfObjectives()) ;
        }

        return dominanceTest(solution1, solution2);
    }

    protected int dominanceTest(Solution<?> solutionA, Solution<?> solutionB) {
        int result ;
        boolean aDominates = false ;
        boolean bDominates = false ;

        double valueA, valueB;
        int numberOfObjectives = solutionA.getNumberOfObjectives();
        Set<Integer> chosenObjectives = getRandomIntegers(numberOfObjectives);

        for (int i : chosenObjectives) {
            valueA = solutionA.getObjective(i);
            valueB = solutionB.getObjective(i);
            if (valueA < valueB) {
                aDominates = true ;
            } else if (valueB < valueA) {
                bDominates = true ;
            } //else equal -- neither dominates.

        }

        if (aDominates == bDominates) {
            /** Both are dominating */
            result = 0;
        } else if (aDominates) {
            /** A better than B */
            result = -1;
        } else {
            /** B better than A */
            result = 1;
        }
        return result ;
    }

    public Set<Integer> getRandomIntegers(int numberOfObjectives) {

        if (choices > numberOfObjectives) {
            return new HashSet<>(IntStream.range(0,numberOfObjectives).boxed().collect(Collectors.toList()));
        } else {
            List<Integer> list =IntStream.range(0,numberOfObjectives).boxed().collect(Collectors.toList());
            Collections.shuffle(list);
            return new HashSet<>(list.subList(0,choices));
        }
    }
}
