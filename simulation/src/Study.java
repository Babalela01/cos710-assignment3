//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import algorithm.CulturalBuilder;
import algorithm.CustomMOEABuilder;
import algorithm.CustomNSGAIIBuilder;
import algorithm.CustomPAESBuilder;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.moead.MOEADBuilder;
import org.uma.jmetal.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.multiobjective.dtlz.DTLZ1;
import org.uma.jmetal.problem.multiobjective.dtlz.DTLZ2;
import org.uma.jmetal.problem.multiobjective.dtlz.DTLZ3;
import org.uma.jmetal.problem.multiobjective.wfg.WFG6;
import org.uma.jmetal.problem.multiobjective.wfg.WFG7;
import org.uma.jmetal.qualityindicator.impl.*;
import org.uma.jmetal.qualityindicator.impl.hypervolume.WFGHypervolume;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.experiment.Experiment;
import org.uma.jmetal.util.experiment.ExperimentBuilder;
import org.uma.jmetal.util.experiment.component.*;
import org.uma.jmetal.util.experiment.util.TaggedAlgorithm;
import problems.DTLZ2B;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Example of experimental study based on solving the ZDT problems with four versions of NSGA-II, each
 * of them applying a different crossover probability (from 0.7 to 1.0).
 * <p>
 * This experiment assumes that the reference Pareto front are not known, so the names of files containing
 * them and the directory where they are located must be specified.
 * <p>
 * Six quality indicators are used for performance assessment.
 * <p>
 * The steps to carry out the experiment are:
 * 1. Configure the experiment
 * 2. Execute the algorithms
 * 3. Generate the reference Pareto fronts
 * 4. Compute the quality indicators
 * 5. Generate Latex tables reporting means and medians
 * 6. Generate Latex tables with the result of applying the Wilcoxon Rank Sum Test
 * 7. Generate Latex tables with the ranking obtained by applying the Friedman test
 * 8. Generate R scripts to obtain boxplots
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class Study {
    private static final int INDEPENDENT_RUNS = 20;

    private static int[] objectiveCounts = new int[]{4, 6, 8, 10};
    //private static int[] objectiveCounts = new int[]{10};


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        int numberOfCores = Integer.parseInt(args[0]);

        for (int m: objectiveCounts) {
            List<DoubleProblem> problemList = new ArrayList<>(6*4);
            problemList.addAll(getProblems(m));

            String experimentBaseDirectory = "Experiments";
            String referenceFrontDirectory = "Fronts\\Fronts_" + m;
            List<TaggedAlgorithm<List<DoubleSolution>>> algorithmList = configureAlgorithmList(problemList, INDEPENDENT_RUNS);

            Experiment<DoubleSolution, List<DoubleSolution>> experiment =
                    new ExperimentBuilder<DoubleSolution, List<DoubleSolution>>("Study_" + m)
                            .setAlgorithmList(algorithmList)
                            .setProblemList(problemList.stream().collect(Collectors.toList()))
                            .setExperimentBaseDirectory(experimentBaseDirectory)
                            .setOutputParetoFrontFileName("FUN")
                            .setOutputParetoSetFileName("VAR")
                            .setReferenceFrontDirectory(referenceFrontDirectory)
                            .setIndicatorList(Arrays.asList(
                                    new InvertedGenerationalDistance<>(),
                                    // new GeneralizedSpread<>(),
                                    // new PISAHypervolume<>(),
                                    // new WFGHypervolume<>(),
                                    new Epsilon<>(),
                                    new Spread<>()
                                    //  new InvertedGenerationalDistancePlus<>()
                            ))
                            .setIndependentRuns(INDEPENDENT_RUNS)
                            .setNumberOfCores(numberOfCores)
                            .build();
            //new ExecuteAlgorithms<>(experiment).run();
//
             new GenerateReferenceParetoSetAndFrontFromDoubleSolutions(experiment).run();
             new ComputeQualityIndicators<>(experiment).run();
            new GenerateLatexTablesWithStatistics(experiment).run();
            new GenerateFriedmanTestTables<>(experiment).run();
            new GenerateWilcoxonTestTablesWithR<>(experiment).run();
            new GenerateBoxplotsWithR<>(experiment).setRows(3).setColumns(3).run(); //*/
        }
    }


    private static List<DoubleProblem> getProblems(int m) throws ClassNotFoundException {
        int wk = 2 * (m - 1);
        int dk2 = m + 10 - 1;
        int dk1 = m + 5 - 1;
        return Arrays.asList(
                new DTLZ1(dk1, m),
                new DTLZ2(dk2, m),
                new DTLZ3(dk2, m),
                new DTLZ2B(dk2, m),
                new WFG6(wk,4,m),
                new WFG7(wk,4,m)
        );
    }

   static List<TaggedAlgorithm<List<DoubleSolution>>> configureAlgorithmList(
            List<DoubleProblem> problemList,
            int independentRuns) {
        int initialCapacity = independentRuns * problemList.size();
        List<TaggedAlgorithm<List<DoubleSolution>>> nsga2 = new ArrayList<>(initialCapacity);
        List<TaggedAlgorithm<List<DoubleSolution>>> ca = new ArrayList<>(initialCapacity);
        List<TaggedAlgorithm<List<DoubleSolution>>> paes = new ArrayList<>(initialCapacity);
        List<TaggedAlgorithm<List<DoubleSolution>>> moead = new ArrayList<>(initialCapacity);

        for (int run = 0; run < independentRuns; run++) {
            for (DoubleProblem problem : problemList) {
                Algorithm<List<DoubleSolution>> algorithm = new CustomNSGAIIBuilder(
                        problem,
                        new SBXCrossover(0.9, 20.0),
                        new PolynomialMutation(problem, 20.0))
                        .setMaxEvaluations(250)
                        .build();
                nsga2.add(new TaggedAlgorithm<>(algorithm, "NSGAII_" + problem.getNumberOfObjectives(), problem, run));

                algorithm = (new CulturalBuilder(problem, 35000, 10, 100, 1.0, 2)).build();
                ca.add(new TaggedAlgorithm<>(algorithm, "CA_" + problem.getNumberOfObjectives(), problem, run));

                algorithm = (new CustomPAESBuilder(problem))
                        .setMaxEvaluations(25000)
                        .setArchiveSize(100)
                        .setBiSections(2)
                        .setMutationOperator(new PolynomialMutation(problem, 20.0))
                        .build();
                paes.add(new TaggedAlgorithm<>(algorithm, "PAES_" + problem.getNumberOfObjectives(), problem, run));

                algorithm = new CustomMOEABuilder(problem, MOEADBuilder.Variant.MOEAD).build();
                moead.add(new TaggedAlgorithm<>(algorithm, "MOEAD_" + problem.getNumberOfObjectives(), problem, run));

            }
        }

        List<TaggedAlgorithm<List<DoubleSolution>>> algorithms = new ArrayList<>(initialCapacity*4);
        algorithms.addAll(ca);
        algorithms.addAll(nsga2);
        algorithms.addAll(paes);
        algorithms.addAll(moead);
        return algorithms;
    }
}