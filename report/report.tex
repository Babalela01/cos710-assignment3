\documentclass[10pt,a4paper,conference]{IEEEtran}
%\usepackage[margin=0.5in]{geometry}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
%\usepackage{placeins}
\usepackage{multirow}
\usepackage{cite}
\usepackage{verbatim}
\usepackage{underscore}
\usepackage{colortbl}
\usepackage{amssymb}
\usepackage[table*]{xcolor}
\usepackage[hidelinks]{hyperref}

\xdefinecolor{gray95}{gray}{0.65}
\xdefinecolor{gray25}{gray}{0.8}

\title{Scalability of Multi-objective Algorithms \\
	\hfil	\\
	\large COS710 Assignment 3}
\author{Renette Ros 13007557}
\date{\today}

\begin{document}
	\maketitle
\begin{abstract}
	This research evaluates the scalability of a number of evolutionary algorithms for solving multi-objective problems. The scalability is evaualted by running the problems on some functions from the DTLZ and WFG toolkits with 4,6,8 and 10 objectives. The  evolutionary algorithms NSGA-II, PAES, MOEA/D-DE and a cultural algorithm were comapred based on inverse generational distance, solution spread and the epsilon quality measure. The study found that the NSGAII does not scale well for any of the chosen quality measures, MOEA/D performs well on many-objective optimisation problems and that there is not a big performance difference between the CA and PAES on problems with more or less objectives. 
\end{abstract}
\section{Introduction}
Multi-objective optimisation problems (MOOPs) are optimisation problems with more than one objective. The solutions to MOOPs are represented as a set of non-dominated (pareto optimal) solutions. Traditionally, MOOPs have been solved with only two or three objectives \cite{purshouse2003evolutionary}. Many objective optimisation algorithms (MaOAs) deal with problems that have more objectives. Similarly to MOOPs the goal is to find the Pareto-optimal set of solutions. 

This research investigate the performance of NSGA-II, PAES, MOEA/D and a cultural algorithm (CA) on many-objective optimisation problems with 4 to 10 objectives. The report is structured as follows: Section \ref{sec:back}  contains background information on the algorithms, Section \ref{sec:impl} discusses implementation details and Section \ref{sec:results} gives the results of study. Section \ref{sec:conclusion} discusses the conclusions reached. 

\section{Background} \label{sec:back}
Multi-objective optimisation problems (MOOPs) are optimisation problems with more than one objective. The solutions to MOOPs are represented as a set of non-dominated (pareto optimal) solutions. A large amount of research has been conducted into evolutionary algorithms for MOOPs. This section  gives an overview of the four algorithms that will be investigated. 

\subsection{NSGA-II}

NSGA-II (non-dominated sorting genetic algorithm II) is an elitism based evolutionary algorithm developed by Deb et al \cite{deb2002fast}. It was created to address common criticisms of the original NSGA (as proposed by Deb and Srinivas \cite{srinivasan1994multi}) and other multi-objective evolutionary algorithms (MOEAs) \cite{deb2002fast}: 
\begin{itemize}
	\item Lack of Elitism
	\item High computational complexity
	\item Need for specifying the sharing parameter
\end{itemize}

This research uses the steady state NSGA-II. Nebro and Durillo \cite{Nebro2009} found that the steady state NSGA-II gives better solutions and converges faster. 

\subsection{PAES}
PAES (The Pareto Archived Evolution Strategy) is an evolution strategy for MOOPs proposed by Knowles and Corne\cite{knowles1999pareto}. The algorithm has a population of only one member but maintains an archive of previous found non-dominating solutions. The objectives in developing PAES was \cite{knowles1999pareto}:
\begin{itemize}
	\item The algorithm should use only local search
	\item All non-dominated solutions should be treated as having equal value. 
\end{itemize}

\subsection{MOEA/D}
MOEA/D, proposed by Zhang and Li, \cite{zhang2007moea} is a multi-objective evolutionary algorithm based on decomposition. MOEA/D decomposes a problem into sub problems that is optimized simultaneously. MOEA/D has lower computational complexity than NSGA-II \cite{zhang2007moea}

\subsection{CA}
Coelho and Becerra \cite{coello2003evolutionary} first proposed a cultural algorithm for MOOPs. Cultural algorithms \cite{reynolds1994introduction} add a belief space to genetic algorithms (or other evolutionary algorithms). The belief space influences how the population space is changed. 

\section{Implementation} \label{sec:impl}
JMetal \cite{Durillo2011760} 5.1 was used for this study. Quality measures were calculated over 30 independent runs. As per the assignment specification, 3 random objectives are selected and used for dominance comparison during selection. In the final pareto ranking all objectives are considered to create the set of non-dominated solutions. 

\subsection{Algorithms}

\subsubsection{NSGA-II}
The variant of NSGA-II that was used is the Jmetal implementation of the steady state NSGA-II. The parameters used are as suggested by the NSGA-II algorithm creators \cite{deb2002fast}:
\begin{itemize}
	\item \textbf{Crossover} Simulated binary crossover with a crossover probability of 0.9 and a distribution index of 20.
	\item \textbf{Mutation} Polynomial mutation  with a distribution index of 20 and a mutation probability of $\frac{1}{n} $ where n is the number of decision variables.
	\item \textbf{Population size} 100 individuals
	\item \textbf{Maximum generation count} 250
\end{itemize}

\subsubsection{PAES}
JMetal 5's PAES algorithm was used. The parameters used for PAES is based ob the suggested values by the PAES creators \cite{knowles1999pareto} and example JMetal studies:
\begin{itemize}
	\item \textbf{Maximum Generations} 25 000
	\item \textbf{Maximum Archive Size} 100
	\item \textbf{Grid subsections} The grid subsections was reduced from 5 to 2 due to memory constraints.
	\item \textbf{Mutation} The same mutation operator as in the NSGA-II is used: Polynomial mutation  with a distribution index of 20 and a mutation probability of $\frac{1}{n} $ where n is the number of decision variables. 
\end{itemize}

\subsection{MOEA/D}
The MOEA/D variation used is the JMetal 5 implementation of MOEA/D-DE described by Zhang and Li \cite{li2009moead}.
\begin{itemize}
	\item \textbf{Population Size} 300
	\item \textbf{Maximum Generations} 150 000
	\item \textbf{Mutation} The same mutation operator as in the NSGA-II is used: Polynomial mutation with a distribution index of 20 and a mutation probability of $\frac{1}{n} $ (where n is the number of decision variables)
	\item \textbf{T} 20
	\item \textbf{$\delta$} 0.1
	\item \textbf{$n_r$}  2
\end{itemize}

\subsection{Cultural Algorithm}
An MOO CA was implemented as described by Reynolds \cite{reynolds1994introduction}. The parameters, except the tournament size, grid subsections and $g_{normative}$, was also set to the values he suggested: 
 \begin{itemize}
 	\item \textbf{Maximum Generations} 35 000
 	\item \textbf{Mutation} A variable is mutated by adding a random value from the distribution $N(0,1)$ to it. The mutation rate is $\frac{1}{n}$ (where n is the number of variables).
 	\item \textbf{Maximum Archive Size} 100
 	\item \textbf{Population Size (p)} 6
 	\item \textbf{Grid subsections ($s_i$)} The grid subsections was reduced from 10 to 2 due to memory constraints. 
 	\item \textbf{$g_{normative}$} (how regularly the normative knowledge is updated) was set to 1 to be able to use a grid already implemented by the JMetal library.
 	\item \textbf{Tournament size (c)} Reynolds \cite{reynolds1994introduction} indicated two contradictory values, $\frac{p}{2}$ and 5, for the tournament size. This research uses $c = \frac{p}{2} = 3 $. 
 \end{itemize}

\subsection{The Problems}
This section describes the six optimization problems that was solved. In these algorithms $M$ is the number of objectives (every problem was evaluated with 4,6,8 and 10 objectives) and $n$ is the number of variables. The existing JMetal implementations of all the problems except DLTZ2B was used. 

\subsubsection{DTLZ}
The DTLZ test suite, developed by Deb et al \cite{deb2002dtlz} can be scaled to any number of  objectives. This reasearch uses DTLZ1, DTLZ2, DTLZ3 and a variation of DTLZ2 (DTLZ2B) with every objective value $i$ scaled by $10^i$.  All parameter values are in the range $[0,1]$. In the DLTZ functions the number of problem variables $n$ is $M+k+1$ with $M$ the number of objectives and k a constant with a value  suggested by the creators \cite{deb2002dtlz}. 

\textbf{DTLZ1} 
\begin{equation}
\begin{split}
f_1 = (1+g)0.5\prod_{i=1}^{M-1}y_i \\
f_{m=2:M-1} = (1+g)0.5(\prod_{i=1}^{M-m}y_i)(1-y_{M-m+1}) \\
f_M = (1+g)0.5(1-y_1) \\
g = 100\left[k + \sum_{i=1}^k((z_i-0.5)^{0.2} -cos(20\pi(z_i -0.5)))\right] \\
with~k = 5
\end{split}
\end{equation}
DTLZ1 is separable and multimodal and the shape of the pareto optimal front is linear \cite{huband2006review}. 

\textbf{DTLZ2}
\begin{equation}
\begin{split}
f_1 = (1+g)\prod_{i=1}^{M-1}cos(y_i\frac{\pi}{2}) \\
f_{m=2:M-1} = (1+g)(\prod_{i=1}^{M-m}cos(y_i\frac{\pi}{2}))sin(y_{M-m+1}\frac{\pi}{2}) \\
f_M = (1+g)sin(y_{1}\frac{\pi}{2})\\
g = \sum_{i=1}^k(z_i-0.5)^2 \\
with~k = 10
\end{split}
\end{equation}
DTLZ2 is separable and unimodal  and the shape of the pareto optimal front is concave \cite{huband2006review}.

\textbf{DTLZ2B} As DTLZ2, but with every objective value $f_m$ scaled by $10^(i-1)$. 

\textbf{DTLZ3} As DTLZ2, but uses $g$ from DTLZ1.
DTLZ3 is separable and multimodal and the shape of the pareto optimal front is concave \cite{huband2006review}.

\subsubsection{WFG}
The WFG toolkit was developed by Huband et al \cite{huband2005scalable,huband2006review} to design scalable multi-objective test problems with specific features. WFG6 and WFG7 was used in this study.  The two WFG functions have 2 parameters besides the number of objectives. The number of position related parameters, $k$ must be divisible by $M-1$ \cite{huband2005scalable} and was set to $2(M-1)$. The number of distance-related parameters $l$ can be any positive integer \cite{huband2005scalable} and was set to 4. 

\textbf{WFG6} WFG6 is non-separable and unimodal. The shape pareto optimal front is concave. \cite{huband2006review}

\textbf{WFG7} WFG7 is separable and unimodal. The shape of the pareto optimal front is concave. \cite{huband2006review}

\subsection{Quality Measures}
The following quality measures were used in this study:
\begin{itemize}
	\item Inverse Generational Distance (IGD) \cite{van1999multiobjective} indicates the average distance from each reference point to the closest solution. A small value suggests good convergence to and distribution over the optimal pareto front. 
	\item Spread \cite{deb2002fast} describes the distribution of the solutions and should be close to zero.
	\item Epsilon (EP) \cite{zitzler2003performance} describes the smallest amount by which the front needs to be translated or scaled to cover the reference pareto front \cite{fonseca2005tutorial}. 
%	\item Inverse Generational Distance Plus \cite{Ishibuchi2015IGD+}
\end{itemize}
These choices were motivated by the existing implementations in the JMetal library and the assignments requirement for the IGD. Reference fronts were generated by JMetal. 

The hypervolume quality indicator was not chosen due to the high computational cost for a large number of objectives. 

\subsection{Empirical Study}
The Mann-Whitney-U test is used to compare the significance of the results.  The function with the best average value of the specific quality measure is compared pair-wise to the other functions by using the quality measure's value over the 30 independent runs as population.  A p-value of 0.05 is used. When the test result has a p-value less than 0.05 it indicates significant differences. 

\section{Results} \label{sec:results}
This section compares the results of the different algorithms on the different number of objectives.  In the result tables highlighted cells shows the smallest values and in the Mann-Whitney-U test tables the highlighted cells indicate significant differences (p \textless 0.05). 

\subsection{NSGA-II}\label{sec:nsgares}
The following tables shows the results of the quality indicators over an average of 30 NSGAII and the results of a Mann-Whitney-U test performed to compare the algorithm with a different amount of objectives. 

\input{include/NSGAII-IGD}
\input{include/NSGAII-IGD-mu}

Table \ref{tbl:NSGAII:IGD} shows the average and standard deviation of the IGD of the NSGAII solutions on the different problems and Table \ref{tbl:NSGAII:IGD:mu} shows the results of a Mann-WHitney-U test performed on the IGDs. The IGD is not consistently best with a specific amount of objectives for all problems. Three of the problems (DTLZ2, DTLZ3 and WFG6) had the best IGD with 4 or 6 (the lower half of objective counts) objectives with significant differences between the best value and all other objective-count variations. 

 %\FloatBarrier

\input{include/NSGAII-EP}
\input{include/NSGAII-EP-mu}
Table \ref{tbl:NSGAII:EP} shows the average and standard deviation of the EP values of the NSGAII solutions on the different problems and Table \ref{tbl:NSGAII:EP:mu} shows the results of a Mann-Whitney-U test performed on the EPs.  Similarly to IGD there is no objective count that performs better across all functions, but 3 of the problems (DTLZ1, DTLZ3 and WFG6) have the best performance on the 4-objective variation and for another problem (DTLZ2) the differences between the best variation (6-objectives) and the 4-objective variation is not significant. 

\input{include/NSGAII-SPREAD}
\input{include/NSGAII-SPREAD-mu}

Table \ref{tbl:NSGAII:SPREAD} shows the averages and standard deviation of the Spread quality measure for the different problems. The majority of the best performing functions had a smaller number of objectives (4 or 6). The best performing DTLZ1 function was the 8-objective variation but the differences between the 8-objective variation and all the other variations were insignificant. 

Based on Tables \ref{tbl:NSGAII:SPREAD} and \ref{tbl:NSGAII:SPREAD:mu} the NSGAII algorithm performs better in terms of spread for problems with a smaller number of objectives. 


%\FloatBarrier
\subsection{PAES}
Tables \ref{tbl:PAES:IGD} and \ref{tbl:PAES:IGD:mu} shows the values of the IGD quality measure and the results of a Mann-U-Whitney test performed on these results. 


\input{include/PAES-IGD}
\input{include/PAES-IGD-mu}

As in the NSGA-II, there is no consistent better IGD performance based on number of objectives. The highest number of best IGDs is in the function variations with 8 and 10 objectives. 
Tables \ref{tbl:PAES:EP} and \ref{tbl:PAES:EP:mu} show the results of the epsilon quality measure. 


\input{include/PAES-EP}
\input{include/PAES-EP-mu}

Table \ref{tbl:PAES:EP} shows that for 3 of the functions (DTLZ1, DTLZ3 and WFG7) the Epsilon measure is the best for 10 objectives. The Mann-Whitney-U test showed that the differences between the best variant and the other variants of each problem were only significant for DTLZ2 and the 4-objective variant of DTLZ3. 


Tables \ref{tbl:PAES:SPREAD} and \ref{tbl:PAES:SPREAD:mu} show the results of applying the spread quality measure to the solutions obtained by PAES. 


\input{include/PAES-SPREAD}

The function variants with more objectives had a better average spread - the best values are all 8-objective and 10-objective variants. Table \ref{tbl:PAES:SPREAD:mu} shows that these differences are only significant for the best values (10-objective variation) of DTLZ2 and DTLZ2B compared to the 4 and 6 objective versions. 

 
 \input{include/PAES-SPREAD-mu}
 
A non-statistical problem encountered when using the PAES on problems with a large amount of objectives was that the arhive uses a hypercube to maintain a good spread. In the JMetal implementation the hypercube is represented by a multi-dimensional array. For problems with many objectives this hypercube can take up a large amount of memory. 
% \FloatBarrier
% \FloatBarrier
\subsection{Cultural Algorithm (CA)}
This section describes the results of the Cultural Algorithm on the six optimisation problems.

\input{include/CA-IGD}
\input{include/CA-IGD-mu}

Based on Table \ref{tbl:CA:IGD}; the CA seems to scale better than the previous algorithms with the best IGD values at the functions with a larger number of objectives; however, the Mann-Whitney-U test (Table \ref{tbl:CA:IGD:mu}
indicates that the differences are not significant between any of the variations of DTLZ1 or DTLZ2, and is significant for the 6,8 and 10 variations of DTLZ3 (which had the best average IGD located at 4 objectives) 


\input{include/CA-EP}
\input{include/CA-EP-mu}

The average epsilon values are not consistently, over all problems, better for a specific amount of objectives (Table \ref{tbl:CA:EP}). The only case where the difference between the best value and every other variation is significant is the DTLZ3 problem, the same problem that the best IGD value at 4 objectives.  


\input{include/CA-SPREAD}

Unlike the previous algorithms the CA has better spread results for a smaller number of objectives. The difference between the best spread and the spread of function variations with a different number of objectives are; however, only significant for DTLZ2B with 10 objectives and DTLZ3. 

\input{include/CA-SPREAD-mu}

The cultural algorithm uses the same grid-based archive as the PAES algorithm. This grid causes very high memory usage for problems with many objectives.   

\subsection{MOEA/D}

This section details the results obtained by the MOEA/D algorithm for the problems and the different number of objectives. 

\input{include/MOEAD-IGD}
\input{include/MOEAD-IGD-mu}

As in the previous algorithms, the IGD values indicated by \ref{tbl:MOEAD:IGD} are not consistently good for a specific number of objectives and there are significant differences between most of the values (Table \ref{tbl:MOEAD:IGD:mu}). 

\input{include/MOEAD-EP}
\input{include/MOEAD-EP-mu}

Table \ref{tbl:MOEAD:EP} shows that most of the best epsilon values are concentrated at the function variants with 8 and 10 objectives. The Mann-Whitney-U test results in Table \ref{sec:back} shows that the difference between the best variant and the other variants is significant for most of the functions. 


\input{include/MOEAD-SPREAD}
\input{include/MOEAD-SPREAD-mu}

Table \ref{tbl:MOEAD:SPREAD} shows, as in the previous algorithms that most of the best spreads are in the functions with the higher number of objectives. Table \ref{tbl:MOEAD:SPREAD:mu} shows that, except for WFG6, most of the results are not significantly different from variations with a different amount of objectives. 

%\FloatBarrier
\section{Conclusion} \label{sec:conclusion} 
Table \ref{tbl:results} summarizes the results obtained in the previous sections. The table indicates the number of best values in each half (4 and 6 or 8 and 10 objective) that are significantly better than both values in the other half. 
\begin{table}
	\centering
	\caption{Summary: Number of significant 'wins'} \label{tbl:results}
	\begin{tabular} {l|cc|cc|cc}
     &IGD &&EP &&Spread &\\
			   & L & U & L & U & L & U \\
			   \hline
	   NSGA-II &  3 & 2  & 3 & 1  & 5  & 0 \\
	   MOEA/D & 2 & 3 & 0 & 4 & 0 & 1\\
	   PAES & 1 & 2 & 1 & 0 & 0 & 2\\
	   CA & 1 & 1 & 1 & 0 & 1 & 0\\
	   \hline
	\end{tabular}
\end{table}

As summarized in Table \ref{tbl:results}, the steady state NSGA-II performed better for the 4 and 6 objective functions than the 8 and 10 objective functions (based on the evaluated quality measures). The biggest difference is the spread quality measure where no function in the upper half had more significant results than the lower half functions. 

The MOEA/D function had a smaller epsilon measure (less translation neccesary to get to optimal front) for 8 and 10 objectives than for 4 and 6. The IGD and spread measures also has a small difference between the two halves, favouring the problems with more objectives. Based on these results the MOEA/D algorithm scales better for many objective problems than NSGA-II. 

The two archive based algorithms, PAES and CA have differences between some of the quality measures but not as big as the 5-0 and 0-4 of the previous two algorithms. This shows that the differences in solution quality are not very significant for the different numbers of objectives compared. As  discussed in the results section, both these functions can however become very computationally expensive as the number of objectives increase due to the m-dimensional grid/hypercube used in their solution archives. Other implementations can handle the hypercube differently.
 


\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}