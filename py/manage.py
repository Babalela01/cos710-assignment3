import statistics
from itertools import product
from operator import index

from scipy.stats import mannwhitneyu

objs = [4, 6, 8, 10]
algos = ["NSGAII", "MOEAD", "PAES", "CA"]
probs = [
    "DTLZ1",
    "DTLZ2",
    "DTLZ2B",
    "DTLZ3",
    "WFG6",
    "WFG7",
]

measures = ["IGD", "EP", "SPREAD"]
path = "../simulation/Experiments/"


def getname(alg, prob, o, measure):
    return path + "Study_%d/data/%s_%d/%s/%s" % (o, alg, o, prob, measure)


stats_line = r"{:s} & {:s} & {:s} & {:s} & {:s} \\" + "\n"
stats_subline = "$%6.3f_{%6.3f}$"


def get_stats_subline(mean, dev, m):
    sl = stats_subline % (mean, dev)
    if mean == m:
        sl = "\cellcolor{gray25}" + sl
    return sl


def get_stats_line(data, prob):
    means = [statistics.mean(data[v]) for v in data]
    devs = [statistics.stdev(data[v]) for v in data]
    m = min(means)
    global best_i
    best_i = objs[means.index(m)]
    lines = [get_stats_subline(mean, dev, m) for (mean, dev) in zip(means, devs)]
    return stats_line.format(prob, *lines)


mu_line = r"{:s} & {:s} & {:s} & {:s} & {:s} \\" + "\n"
mu_subline = "%5.3f"


def mu(x1, x2):
    muw = mannwhitneyu(x1, x2)
    p = muw[1] * 2
    return p


def get_mu_subline(o1, o2):
    if o1 == o2:
        return " -- "
    val = mu(data[o1], data[o2])
    sl = mu_subline % val
    if val < 0.05:
        sl = r"\cellcolor{gray25}" + sl
    return sl


def get_mu_line(data, prob):
    keys = product([best_i] * 4, objs)
    results = [get_mu_subline(o1, o2) for o1, o2 in keys]
    return mu_line.format(prob, *results)


for measure in measures:
    for alg in algos:
        with open("../report/include/" + alg + "-" + measure + ".tex", "w") as stats_file, open(
                                                "../report/include/" + alg + "-" + measure + "-mu.tex", "w") as mu_file:
            lines_stats = [
                r"\begin{table}[!h]",
                r"\centering",
                r"\caption{ " + alg + " - " + measure + " Average and Standard Deviation }",
                r"\label{tbl:%s:%s}" % (alg, measure),
                r"\begin{tabular} {l r r r r }",
                r"Objectives & 4 & 6 & 8 & 10 \\",
                r"\hline " + "\n"]
            stats_file.write("\n".join(lines_stats))

            lines_mu = [
                r"\begin{table}[!h]",
                r"\centering",
                r"\caption{ " + alg + " - " + measure + " - Mann-Whitney-U p-values }",
                r"\label{tbl:%s:%s:mu}" % (alg, measure),
                r"\begin{tabular} { l | r r r r }",
                r"  & 4 & 6 & 8 & 10 \\",
                r"\hline " + "\n"]
            mu_file.write("\n".join(lines_mu))

            for prob in probs:
                data = {}
                best_i = -1
                for o in objs:
                    with open(getname(alg, prob, o, measure)) as f:
                        data[o] = [*map(float, f.readlines())]

                stats_file.write(get_stats_line(data, prob))
                mu_file.write(get_mu_line(data, prob))

            stats_file.write(r"\end{tabular} \end{table}")
            mu_file.write(r"\end{tabular} \end{table}")
